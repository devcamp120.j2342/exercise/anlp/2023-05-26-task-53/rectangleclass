import models.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.0f, 3.0f);

        System.out.println(rectangle1.toString());
        System.out.println("diện tích:" + rectangle1.getArea());
        System.out.println("chu vi:" + rectangle1.getPerimeter());


        System.out.println(rectangle2.toString());
        System.out.println("diện tích: " + rectangle2.getArea());
        System.out.println( "chu vi: " + rectangle2.getPerimeter());
    }
}
